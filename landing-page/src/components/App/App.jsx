import React, { Component } from 'react';

import Header from '../Header/Header.jsx';
import Form from '../Form/Form.jsx';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Form action="#url"/>
      </div>
    );
  }
}

export default App;
