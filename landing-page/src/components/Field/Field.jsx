import React, { Component } from 'react';

import './Field.css';

class Field extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '', emptyClass: '', validClass: '', errorClass: '' };
    this.requiredClass = this.props.required ? 'required' : '';
    this.isSelect = this.props.type === 'select';

    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }
  handleBlur(event) {
    let value = event.target.value;
    let state = Object.assign({}, this.state);
    if (value === '' || value.trim() === '') {
      state.value = '';
      state.emptyClass = 'empty';
      state.validClass = '';
      state.errorClass = '';
    } else {
      state.emptyClass = '';
    }
    if (event.target.checkValidity() && !state.emptyClass) {
      state.validClass = 'valid';
      state.errorClass = '';
    } else {
      state.validClass = '';
      if (this.props.required) state.errorClass = 'error';
    }
    this.setState(state);
  }
  handleChange(event) {
    let value = event.target.value;
    this.setState({ value: value });
  }
  handleSelect(event) {
    let select = event.target;
    let emptyOption = select.querySelector('option[value=""]');
    if (emptyOption) {
      emptyOption.nextElementSibling.selected = true;
      select.removeChild(emptyOption);
    }
  }
  render() {
    let input =
      <input className={`${this.state.emptyClass} ${this.state.validClass} ${this.state.errorClass}`} type={this.props.type} name={this.props.name} id={this.props.name} pattern={this.props.pattern} value={this.state.value} onChange={this.handleChange} onBlur={this.handleBlur} required={this.props.required}/>;

    let select =
      <select className={`${this.state.emptyClass} ${this.state.validClass} ${this.state.errorClass}`} name={this.props.name} id={this.props.name} onBlur={this.handleBlur} onTouchStart={this.handleSelect} onMouseDown={this.handleSelect} onFocus={this.handleSelect} required={this.props.required}>
        <option value="" defaultValue></option>
        {this.props.children}
      </select>;

    let field = this.isSelect ? select : input;

    return (
      <div className={`field ${this.requiredClass} ${this.isSelect ? 'select' : ''}`}>
        {field}
      	<label htmlFor={this.props.name}>{this.props.placeholder}</label>
      </div>
    );
  }
}

export default Field;