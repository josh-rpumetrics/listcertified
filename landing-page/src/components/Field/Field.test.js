import React from 'react';
import { shallow } from 'enzyme';
import Field from './Field.jsx';

describe('Field Component', () => {
  it('renders without crashing', () => {
    shallow(<Field />);
  });
  it('renders label correctly', () => {
    const wrapper = shallow(<Field name="test" placeholder="Test"/>);
    const label = <label htmlFor="test">Test</label>
    expect(wrapper.contains(label)).toEqual(true);
  });
  it('renders the input type correctly', () => {
    const wrapper = shallow(<Field type="text"/>);
    expect(wrapper.find('input').prop('type')).toEqual('text');
  });
  it('renders select field', () => {
    const wrapper = shallow(<Field type="select"></Field>);
    expect(wrapper.find('select').type()).toEqual('select');
  });
  it('renders options in select', () => {
    const wrapper = shallow(<Field type="select"><option value="test">Test</option></Field>);
    expect(wrapper.find('select').contains(<option value="test">Test</option>)).toEqual(true);
  });

});