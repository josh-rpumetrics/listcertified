import React, { Component } from 'react';

import Field from '../Field/Field.jsx';
import './Form.css';
import lock_image from './lock.png';

class Form extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();

    let form = event.target;
    let url = form.action;
    let formData = new FormData(form);
    let request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {

      }
    }
    request.open('POST', url, true);
    request.send(formData);
  }
  render() {
    return (
      <div className="form">
        <div className="form--instructions">Validate your first list <span>- <strong>FREE</strong><sup>1</sup></span></div>
        <form action={this.props.action} onSubmit={this.handleSubmit}>
          <Field type="text" name="fname" placeholder="First Name" required/>
          <Field type="text" name="lname" placeholder="Last Name" required/>
          <Field type="text" name="cname" placeholder="Company Name"/>
          <Field type="email" name="email" placeholder="Email" required/>
          <Field type="tel" name="phone" pattern="^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$" placeholder="Phone" required/>
          <Field type="select" name="est-list-size" placeholder="Estimated List Size">
            <option value="test1">Test 1</option>
            <option value="test2">Test 2</option>
            <option value="test3">Test 3</option>
          </Field>
          <button className="form--button" type="submit">Get Started</button>
        </form>
        <div className="form--secure"><img src={lock_image} alt="" draggable="false"/>Your information is 100% secure and will never be shared with anyone. <a href="">Privacy Policy</a></div>
        <div className="form--details"><sup>1</sup>Free first list validation limited to 500 records</div>
      </div>
    );
  }
}

export default Form;
