import React, { Component } from 'react';

import './Header.css';
import Headline from '../Headline/Headline.jsx';
import logo from './logo.png';

class Header extends Component {
  render() {
    return (
      <div className="header">
        <img className="header--logo" src={logo} alt="ListCertified"/>
        <Headline/>
      </div>
    );
  }
}

export default Header;