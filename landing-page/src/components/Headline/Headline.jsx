import React, { Component } from 'react';

import './Headline.css';

class Headline extends Component {
  render() {
    return (
      <div className="headline">
        <h1>Data Validation For Up to <span>70% LESS!</span></h1>
        <ul>
          <li>Industry Leading Scoring</li>
          <li>Live Email Verification</li>
          <li>Dedicated Support</li>
          <li>Easy to use</li>
        </ul>
      </div>
    );
  }
}

export default Headline;